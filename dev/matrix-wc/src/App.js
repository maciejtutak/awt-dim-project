import React, {Component} from 'react';
import {Router} from '@reach/router';
import {Provider} from 'react-redux';
import store from './store';

import {MainView, LoginView, RegisterView} from './Views';
import Authorized from './Components/Authorized'

import './App.css';

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router>
                    <LoginView path="login"/>
                    <RegisterView path="register"/>
                    <Authorized path="/">
                        <MainView path="/"/>
                    </Authorized>
                </Router>
            </Provider>
        );
    }
}

export default App;
