import {api} from '../apiEndpoints';
import {handleResponse} from '../helpers';

export const roomCreationServices = {
    createRoom
};

function createRoom(accessToken, alias) {
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({room_alias_name: alias})
    };

    const serverUrl = localStorage.getItem("homeserver");

    return fetch(`${serverUrl}${api}createRoom?access_token=${accessToken}`, requestOptions)
        .then(handleResponse)
}

