import { api } from "../apiEndpoints";
import { handleResponse, generateqQuickTId } from "../helpers";

export const roomEventServices = {
  send,
  getRoomMessages,
  getRoomMembers
};

function send(accessToken, roomId, message) {
  const requestOptions = {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ msgtype: "m.text", body: message })
  };

  const tId = generateqQuickTId();
  const serverUrl = localStorage.getItem("homeserver");

  return fetch(
    `${serverUrl}${api}rooms/${roomId}/send/m.room.message/${tId}?access_token=${accessToken}`,
    requestOptions
  ).then(handleResponse);
}

// polling f, reading previous b
function getRoomMessages(accessToken, roomId, from = "", dir = "f") {
  const requestOptions = {
    method: "GET",
    headers: { "Content-Type": "application/json" }
  };
  const serverUrl = localStorage.getItem("homeserver");

  return fetch(
    `${serverUrl}${api}rooms/${roomId}/messages?from=${from}&dir=${dir}&limit=20&access_token=${accessToken}`,
    requestOptions
  ).then(handleResponse);
}

function getRoomMembers(accessToken, roomId) {
  const requestOptions = {
    method: "GET",
    headers: { "Content-Type": "application/json" }
  };
  const serverUrl = localStorage.getItem("homeserver");

  return fetch(
    `${serverUrl}${api}rooms/${roomId}/members?access_token=${accessToken}`,
    requestOptions
  ).then(handleResponse);
}
