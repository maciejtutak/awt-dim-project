import { api } from "../apiEndpoints";
import { handleResponse } from "../helpers";

export const roomParticipationServices = {
  invite,
  join,
  leave
};

function invite(accessToken, roomId, userId) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ user_id: userId })
  };
  const serverUrl = localStorage.getItem("homeserver");

  return fetch(
    `${serverUrl}${api}rooms/${roomId}/invite?access_token=${accessToken}`,
    requestOptions
  ).then(handleResponse);
}

function join(accessToken, roomId) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" }
  };
  const serverUrl = localStorage.getItem("homeserver");

  return fetch(
    `${serverUrl}${api}rooms/${roomId}/join?access_token=${accessToken}`,
    requestOptions
  ).then(handleResponse);
}

function leave(accessToken, roomId) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" }
  };

  const serverUrl = localStorage.getItem("homeserver");
  return fetch(
    `${serverUrl}${api}rooms/${roomId}/leave?access_token=${accessToken}`,
    requestOptions
  ).then(handleResponse);
}
