import { api } from "../apiEndpoints";
import { handleResponse } from "../helpers";

//TODO: change for bearer authorization

export const sessionManagementServices = {
  // checkAuthFlow,
  login,
  logout
};

// function checkAuthFlow() {
//     const requestOptions = {
//         method: 'GET',
//         headers: {'Content-Type': 'application/json'},
//     };
//     return fetch(`${serverUrl}${apiLogin}`, requestOptions)
//         .then(handleResponse)
// }

function login(user, password, type = "m.login.password") {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ user, password, type })
  };
  const serverUrl = localStorage.getItem("homeserver");
  console.log(`${serverUrl}${api}login`);
  return fetch(`${serverUrl}${api}login`, requestOptions)
    .then(handleResponse)
    .then(user => {
      localStorage.setItem("user", JSON.stringify(user));
      return user;
    });
}

function logout(accessToken) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" }
  };
  const serverUrl = localStorage.getItem("homeserver");
  return fetch(
    `${serverUrl}${api}logout?access_token=${accessToken}`,
    requestOptions
  )
    .then(handleResponse)
    .then(user => {
      localStorage.removeItem("user");
      return user;
    });
}
