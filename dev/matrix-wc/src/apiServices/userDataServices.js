import { api } from "../apiEndpoints";
import { handleResponse } from "../helpers";

export const userDataServices = {
  register
};

function register(username, password, auth = { type: "m.login.dummy" }) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-type": "application/json" },
    body: JSON.stringify({ username, password, auth })
  };
  const serverUrl = localStorage.getItem("homeserver");
  console.log(`${serverUrl}${api}register`);
  return fetch(`${serverUrl}${api}register`, requestOptions)
    .then(handleResponse)
    .then(user => {
      localStorage.setItem("user", JSON.stringify(user));
      return user;
    });
}
