import { api } from "../apiEndpoints";
import { handleResponse } from "../helpers";

export const syncDataServices = {
  sync
};

function sync(accessToken, nextBatch = "") {
  const requestOptions = {
    method: "GET",
    headers: { "Content-Type": "application/json" }
  };

  const since = (nextBatch !== "") ? `&since=${nextBatch}` : "";
  const serverUrl = localStorage.getItem("homeserver");

  return fetch(
    `${serverUrl}${api}sync?filter={"room":{"timeline":{"limit":0}}}${since}&access_token=${accessToken}`,
    requestOptions
  ).then(handleResponse);
}
