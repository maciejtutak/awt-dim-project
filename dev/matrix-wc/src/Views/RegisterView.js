import React, {Component} from 'react';
import {connect} from 'react-redux';
import {userDataActions} from '../store/actions';
import {Link} from '@reach/router';

class RegisterViewDisconnected extends Component {
    state = {
        username: '',
        password1: '',
        password2: '',
    };

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value,
        })
    };

    validatePassword = (password1, password2) => {
        return (password1 === password2)
    };

    handleClick = event => {
        event.preventDefault();
        const {register} = this.props;
        const {username, password1, password2} = this.state;
        if (this.validatePassword(password1, password2)) {
            register(username, password1);
        } else {
            // error
        }
    };

    render() {
        return (
            <div>
                <h1>RegisterView</h1>
                <form onChange={this.handleChange}>
                    <input type="text" id="username" placeholder="Username"/>
                    <input type="password" id="password1" placeholder="Password"/>
                    <input type="password" id="password2" placeholder="Password"/>
                    <input type="button" value="Register" onClick={this.handleClick}/>
                    <Link to='/login'>login</Link>
                </form>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        register: (username, password) => {
            dispatch(userDataActions.register(username, password))
        },
    }
};

export const RegisterView = connect(null, mapDispatchToProps)(RegisterViewDisconnected);

