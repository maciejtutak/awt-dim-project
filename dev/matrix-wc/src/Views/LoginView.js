import React, { Component } from "react";
import { connect } from "react-redux";
import { sessionManagementActions, userDataActions } from "../store/actions";
import styled from "styled-components";

import { ReactComponent as Logo } from "../assets/matrixLogo.svg";
import { userSettingsActions } from "../store/actions/userSettingsActions";

class LoginViewDisconnected extends Component {
  state = {
    homeserver: "http://synapse.a.localhost/",
    user: "",
    password: "",
    password1: "",
    password2: ""
  };

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  handleLogin = event => {
    event.preventDefault();
    const { login } = this.props;
    localStorage.setItem("homeserver", this.state.homeserver);
    login(this.state.user, this.state.password);
  };

  validatePassword = (password1, password2) => {
    return password1 === password2;
  };

  handleRegister = event => {
    event.preventDefault();
    const { register } = this.props;
    localStorage.setItem("homeserver", this.state.homeserver);
    const { username, password1, password2 } = this.state;
    if (this.validatePassword(password1, password2)) {
      register(username, password1);
    } else {
      // error
    }
  };

  render() {
    return (
      <Layout>
        <Title>Pigeon.</Title>
        <SubTitle>
          <h2>Simple web client for</h2>
          <Logo />
        </SubTitle>
        <Container>
          <Step>1. Choose your homeserver</Step>
            <select id="homeserver" value={this.state.homeserver} onChange={this.handleChange}>
              <option value="http://synapse.a.localhost/">synapse.a.localhost</option>
              <option value="http://synapse.b.localhost/">synapse.b.localhost</option>
            </select>
          <Step>2. Enjoy</Step>
          <FormsContainer>
            <Form onChange={this.handleChange}>
              <Input type="text" id="user" placeholder="Username" />
              <Input type="password" id="password" placeholder="Password" />
              <button value="Login" onClick={this.handleLogin}>
                Login
              </button>
            </Form>
            <Big>or</Big>
            <Form onChange={this.handleChange}>
              <Input type="text" id="username" placeholder="Username" />
              <Input type="password" id="password1" placeholder="Password" />
              <Input type="password" id="password2" placeholder="Password" />
              <button value="Register" onClick={this.handleRegister}>
                Register
              </button>
            </Form>
          </FormsContainer>
        </Container>
      </Layout>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    login: (user, password) => {
      dispatch(sessionManagementActions.login(user, password));
    },
    register: (username, password) => {
      dispatch(userDataActions.register(username, password));
    }
  };
};

export const LoginView = connect(
  null,
  mapDispatchToProps
)(LoginViewDisconnected);

const Layout = styled.div`
  margin: 50px auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Title = styled.h1`
  font-size: 36px;
  font-weight: 600;
  font-family: var(--font-family);
  color: var(--text-color-dark);
  margin-bottom: 0;
`;

const SubTitle = styled.div`
  display: flex;
  align-items: center;

  & h2 {
    margin-top: 10px;
    margin-right: 10px;
    font-size: 28px;
    font-weight: 400;
    color: var(--text-color);
    text-transform: none;
  }
`;

const Container = styled.div`
  margin: 30px;
  padding: 30px;
  border: 1px solid var(--border-dark);
  border-radius: 2px;
`;

const Step = styled.h3`
  margin: 10px 20px;
  font-size: 24px;
  font-weight: 400;
  color: var(--text-color);
  text-transform: none;
`;

const FormsContainer = styled.div`
  display: flex;
  align-items: center;
`;

const Big = styled.p`
  font-size: 30px;
  font-weight: 600;
  font-family: var(--font-family);
  color: var(--text-color-dark);
  margin: 0 30px;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;

  & > * {
    margin-bottom: 10px;
  }
`;

const Input = styled.input`
  padding: 8px;
  border: 1px solid var(--border-dark);
  border-radius: 2px;
  box-shadow: none;
`;
