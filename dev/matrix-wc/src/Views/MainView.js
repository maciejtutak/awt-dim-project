import React, { Component } from "react";
import { connect } from "react-redux";
import {
  roomCreationActions,
  roomParticipationActions,
  sessionManagementActions,
  syncDataActions
} from "../store/actions";
import styled from "styled-components";
import { RoomList, RoomView } from "../Components/Room";
import { User } from "../Components/User";
import { roomEventActions } from "../store/actions/roomEventActions";
import {
  getCurrentRoom,
  getUsername,
  getCurrentRoomAllowed
} from "../store/selectors";
import { RoomSidebar } from "../Components/Room/RoomSidebar";

class MainViewDisconnected extends Component {
  componentDidMount() {
    const { sync } = this.props;
    sync();
    this.interval = setInterval(() => sync(), 2000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  handleClick = event => {
    event.preventDefault();
    const { logout } = this.props;
    logout();
  };

  // handleClick2 = event => {
  //   event.preventDefault();
  //   const { sync } = this.props;
  //   sync();
  // };

  handleClick3 = event => {
    event.preventDefault();
    const { createRoom } = this.props;
    const alias = window.prompt("alias?");
    createRoom(alias);
  };

  // handleClick4 = event => {
  //   event.preventDefault();
  //   const { invite } = this.props;
  //   const userId = window.prompt("user?");
  //   invite(userId);
  // };

  // handleClick5 = event => {
  //   event.preventDefault();
  //   const { join } = this.props;
  //   join();
  // };

  // handleClick6 = event => {
  //   event.preventDefault();
  //   const { leave } = this.props;
  //   leave();
  // };

  // handleClick7 = event => {
  //   event.preventDefault();
  //   const { getRoomMessagesF } = this.props;
  //   getRoomMessagesF();
  // };

  // handleClick8 = event => {
  //   event.preventDefault();
  //   const { getRoomMessagesB } = this.props;
  //   getRoomMessagesB();
  // };

  render() {
    const { username, currentRoomId, allowed } = this.props;
    return (
      <Layout>
        <Menu>
          <User />
          <button onClick={this.handleClick}>logout</button>
          <button onClick={this.handleClick3}>create room</button>
          {/* <button onClick={this.handleClick2}>sync</button>
          <button onClick={this.handleClick4}>invite</button>
          <button onClick={this.handleClick5}>join</button>
          <button onClick={this.handleClick6}>leave</button>
          <button onClick={this.handleClick7}>get messagesF</button>
          <button onClick={this.handleClick8}>get messagesB</button> */}
        </Menu>
        <Sidebar>
          <RoomList />
        </Sidebar>
        <Info>
          <RoomSidebar />
        </Info>
        <Room>{currentRoomId !== "" && allowed && <RoomView />}</Room>
      </Layout>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentRoomId: getCurrentRoom(state),
    allowed: getCurrentRoomAllowed(state),
    username: getUsername(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: () => {
      dispatch(sessionManagementActions.logout());
    },
    sync: () => {
      dispatch(syncDataActions.sync());
    },
    createRoom: alias => {
      dispatch(roomCreationActions.createRoom(alias));
    },
    invite: userId => {
      dispatch(roomParticipationActions.invite(userId));
    },
    join: () => {
      dispatch(roomParticipationActions.join());
    },
    leave: () => {
      dispatch(roomParticipationActions.leave());
    },
    getRoomMessagesF: () => {
      dispatch(roomEventActions.getRoomMessagesF());
    },
    getRoomMessagesB: () => {
      dispatch(roomEventActions.getRoomMessagesB());
    }
  };
};

export const MainView = connect(
  mapStateToProps,
  mapDispatchToProps
)(MainViewDisconnected);

const Layout = styled.div`
  margin: 0;
  padding: 0;
  display: grid;
  grid-template-columns: 325px 250px auto;
  grid-template-rows: 35px calc(100vh - 35px);
  grid-template-areas:
    "menu menu menu"
    "sidebar info room";
`;

const Menu = styled.div`
  grid-area: menu;
  display: flex;
  flex-flow: row;
  background-color: var(--background-light);
  border-bottom: 1px solid var(--border-dark);
`;

const Sidebar = styled.div`
  grid-area: sidebar;
  background-color: var(--background-light);
  border-right: 1px solid var(--border-dark);
  overflow-y: scroll;
`;

const Info = styled.div`
  grid-area: info;
  background-color: var(--background-light);
  border-right: 1px solid var(--border-dark);
`;

const Room = styled.div`
  grid-area: room;
  background-color: var(--background-color);
`;
