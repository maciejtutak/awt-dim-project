import React, { Component } from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import { getUsername } from "../store/selectors";

    
class UserDisconnected extends Component {
  render() {
    const { username } = this.props;
    return <UserContainer>{username}</UserContainer>;
  }
}

const mapStateToProps = state => {
  return {
    username: getUsername(state)
  };
};

export const User = connect(mapStateToProps, null)(UserDisconnected);

const UserContainer = styled.div`
    margin: 0;
    padding: 0;
    display: flex;
    flex-direction: column;
`

// const User
