import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import {Redirect} from '@reach/router';
import {connect} from 'react-redux';
import {isAuthenticated} from '../store/selectors';

class AuthorizedDisconnected extends Component {
    render() {
        const {isAuthenticated, children} = this.props;
        return isAuthenticated ? <Fragment>{children}</Fragment> : <Redirect to={'/login'} noThrow />
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: isAuthenticated(state)
    }
};


AuthorizedDisconnected.propTypes = {
    children: PropTypes.node.isRequired,
    isAuthenticated: PropTypes.bool,
};

const Authorized = connect(mapStateToProps)(AuthorizedDisconnected);

export default Authorized;
