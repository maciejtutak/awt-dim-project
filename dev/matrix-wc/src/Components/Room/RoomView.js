import React, { Component } from "react";
import styled from "styled-components";
import { MessageInput } from "./MessageInput";
import { MessageView } from "./MessageView";

export class RoomView extends Component {
  render() {
    return (
      <Layout>
        <MessageView />
        <MessageInput />
      </Layout>
    );
  }
}

const Layout = styled.div`
  height: 100%;
  background-color: silver;
  display: flex;
  flex-direction: column;
  overflow-y: hidden;
`;
