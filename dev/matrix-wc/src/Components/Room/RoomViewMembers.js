import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styled from "styled-components";
import { getRoomMembers } from "../../store/selectors";
import { getUsernameFromId } from "../../helpers";

class RoomViewMembersDisconnected extends Component {
  componentDidMount() {}

  componentDidUpdate(prevProps, prevState, snapshot) {}

  render() {
    const { members } = this.props;
    return members.length > 0 ? (
      <Layout>
        <h3>Members</h3>
        <List>
          {members
            .filter(member => member.content.membership === "join")
            .map(member => (
              <li key={member.user_id}>{getUsernameFromId(member.user_id)}</li>
            ))}
        </List>
      </Layout>
    ) : (
      <Layout>
        <h3>Members</h3>
        <List>No room selected.</List>
      </Layout>
    );
  }
}

RoomViewMembersDisconnected.propTypes = {};

const mapStateToProps = state => {
  return {
    members: getRoomMembers(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export const RoomViewMembers = connect(
  mapStateToProps,
  mapDispatchToProps
)(RoomViewMembersDisconnected);

const Layout = styled.div`
  padding: 8px;
  background-color: var(--background-color);
  border-radius: 8px;
  min-height: 100px;
  flex: 1 1;
`;

const List = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
  color: var(--text-color);
  overflow-y: scroll;
  height: calc(100% - 30px);
`;

const ListItem = styled.li``;
