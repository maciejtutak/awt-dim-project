import React, { Component } from "react";
import { connect } from "react-redux";
import styled from "styled-components";

import { roomEventServices } from "../../apiServices/roomEventServices";
import { getCurrentRoom, getUserAccessToken } from "../../store/selectors";

import { ReactComponent as Send } from "../../assets/featherPen.svg";

class MessageInputDisconnected extends Component {
  constructor(props) {
    super(props);
    this.input = React.createRef();
  }
  j
  state = {
    message: ""
  };

  componentDidMount() {
    this.input.current.focus();
  }

  handleChange = event => {
    this.setState({
      message: event.target.value
    });
  };

  // temporary solution just for sending messages
  handleClick = event => {
    const { accessToken, currentRoom } = this.props;
    const { message } = this.state;
    if (message !== "") {
      roomEventServices
        .send(accessToken, currentRoom, message)
        .then(this.setState({ message: "" }));
    }
  };

  handleKeyDown = event => {
    if (event.key === "Enter") {
      event.preventDefault();
      this.handleClick();
    }
  };

  render() {
    return (
      <MessageInputContainer>
        <TextArea
          placeholder={"Type your message here..."}
          value={this.state.message}
          onChange={this.handleChange}
          onKeyDown={this.handleKeyDown}
          ref={this.input}
        />
        <SendButton onClick={this.handleClick}>
          <Send height="50px" width="50px" />
        </SendButton>
      </MessageInputContainer>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentRoom: getCurrentRoom(state),
    accessToken: getUserAccessToken(state),
  };
};

export const MessageInput = connect(mapStateToProps)(MessageInputDisconnected);

const MessageInputContainer = styled.div`
  display: flex;
  padding: 10px;
  flex-direction: row;
  align-items: space-between;
  background-color: white;
  max-height: 200px;
  border-top: 1px solid var(--border-dark);
`;

const TextArea = styled.textarea`
  height: 26px;
  max-height: 200px;
  width: 100%;
  padding: 10px 20px;
  line-height: 1.5;
  box-shadow: none;
  resize: none;
  border-radius: 10px;
  border: 1px solid var(--border-light);
`;

const SendButton = styled.button`
  width: 48px;
  height: 48px;
  background-color: var(--accent-color);
  border: 0;
  border-radius: 10px;
  margin-left: 10px;
  padding: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`;
