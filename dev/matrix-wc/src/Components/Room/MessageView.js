import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import debounce from "lodash/debounce";
import { connect } from "react-redux";
import { roomEventActions } from "../../store/actions/roomEventActions";
import { getUsernameFromId } from "../../helpers";
import {
  getCurrentRoom,
  getRoomMessages,
  getIsLoadingData,
  getUserId,
  getCurrentRoomAllowed
} from "../../store/selectors";

import Message from "./Message";

class MessageViewDisconencted extends Component {
  constructor(props) {
    super(props);
    this.messageView = React.createRef();
  }

  state = {
    shouldStayAtBottom: true
  };

  componentDidMount() {
    const { getRoomMessagesF, currentRoomId } = this.props;
    if (currentRoomId !== "") {
      this.interval = setInterval(() => {
        getRoomMessagesF();
      }, 2000);
    }

    this.stayAtBottom();
  }

  stayAtBottom() {
    const { shouldStayAtBottom } = this.state;
    const el = this.messageView.current;
    if (shouldStayAtBottom) {
      el.scrollTop = el.scrollHeight - el.clientHeight;
    }
  }

  shouldComponentUpdate(nextProps) {
    const { messages, currentRoomId } = this.props;
    if (nextProps.messages.length !== messages.length) {
      return true;
    }
    if (nextProps.currentRoomId !== currentRoomId) {
      return true;
    }
    return false;
  }

  componentDidUpdate(prevProps) {
    const { getRoomMessagesF, currentRoomId } = this.props;
    if (prevProps.currentRoomId !== currentRoomId) {
      clearInterval(this.interval);
      // console.log("CLEAR INTERVAL")
      // console.log("CLEAR INTERVAL")
      // console.log("CLEAR INTERVAL")
      // console.log(this.interval)
      if (currentRoomId !== "") {
        this.interval = setInterval(() => {
          getRoomMessagesF();
        }, 2000);
      }
    }

    // const { messages } = this.props;
    // if (prevProps.messages.length === messages.length) {
    // }
    // console.log(this.state.shouldStayAtBottom);

    this.stayAtBottom();
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  handleScroll = event => {
    const el = event.target;
    const { getRoomMessagesB } = this.props;
    const { shouldStayAtBottom } = this.state;
    if (el.scrollTop === 0 && !shouldStayAtBottom) {
      getRoomMessagesB();
    }
    const atBottom = el.scrollHeight - el.clientHeight <= el.scrollTop + 1;
    this.setState({ shouldStayAtBottom: atBottom });
  };

  render() {
    const { isLoadingData, messages, userId, allowed } = this.props;
    console.log('allowed')
    console.log('allowed')
    console.log('allowed')
    console.log('allowed')
    console.log(allowed)
    console.log(messages);
    return (
      <MessageViewContainer ref={this.messageView} onScroll={this.handleScroll}>
        {/* {isLoadingData && <div>Loading data.</div>} */}
        {allowed && messages.map(message => (
          <Message
            key={message.event_id}
            value={message.content.body}
            sender={
              userId === message.sender ? "" : getUsernameFromId(message.sender)
            }
          />
        ))}
      </MessageViewContainer>
    );
  }
}

MessageViewDisconencted.propTypes = {
  getRoomMessagesB: PropTypes.func.isRequired,
  getRoomMessagesF: PropTypes.func.isRequired,
  currentRoomId: PropTypes.string.isRequired,
  messages: PropTypes.array.isRequired
};

const mapStateToProps = state => {
  return {
    currentRoomId: getCurrentRoom(state),
    messages: getRoomMessages(state),
    isLoadingData: getIsLoadingData(state),
    userId: getUserId(state),
    allowed: getCurrentRoomAllowed(state),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getRoomMessagesB: dir => dispatch(roomEventActions.getRoomMessagesB(dir)),
    getRoomMessagesF: dir => dispatch(roomEventActions.getRoomMessagesF(dir))
  };
};

export const MessageView = connect(
  mapStateToProps,
  mapDispatchToProps
)(MessageViewDisconencted);

const MessageViewContainer = styled.div`
  overflow-y: scroll;
  background-color: var(--background-color);
  padding: 10px;
  display: flex;
  flex-direction: column;
  height: 100%;

  & > * {
    max-width: 80%;
  }
`;

// const MessageViewScroll = styled.div`
//   overflow-y: scroll;
//   background-color: var(--background-color);
//   padding-bottom: 20px;
// `;
