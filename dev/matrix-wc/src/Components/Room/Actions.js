import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import { roomParticipationActions } from "../../store/actions";
import { getCurrentRoom, getCurrentRoomAllowed } from "../../store/selectors";

class ActionsDisconnected extends Component {
  handleJoin = event => {
    event.preventDefault();
    const { join } = this.props;
    join();
  };

  handleClick3 = event => {
    event.preventDefault();
    const { leave } = this.props;
    leave();
  };

  handleClick4 = event => {
    event.preventDefault();
    const { invite } = this.props;
    const userId = window.prompt("user?");
    invite(userId);
  };

  render() {
    const { currentRoom, allowed } = this.props;
    return (
      <ActionBar>
        {allowed && currentRoom !== "" && (
          <Fragment>
            <button onClick={this.handleClick4}>invite</button>
            <button onClick={this.handleClick3}>leave</button>
          </Fragment>
        )}
        {!allowed && currentRoom !== "" && (
          <Fragment>
            <button onClick={this.handleJoin}>join</button>
            <button onClick={this.handleClick3}>leave</button>
          </Fragment>
        )}
        {!allowed && currentRoom === "" && <InfoSpan>Select room.</InfoSpan>}
      </ActionBar>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentRoom: getCurrentRoom(state),
    allowed: getCurrentRoomAllowed(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    invite: userId => {
      dispatch(roomParticipationActions.invite(userId));
    },
    join: () => {
      dispatch(roomParticipationActions.join());
    },
    leave: () => {
      dispatch(roomParticipationActions.leave());
    }
  };
};

export const Actions = connect(
  mapStateToProps,
  mapDispatchToProps
)(ActionsDisconnected);

const ActionBar = styled.div`
  display: flex;
  margin: 0 !important;
`;

const InfoSpan = styled.span`
  color: var(--text-color);
  font-size: 14px;
  font-style: italic;
`;
