import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styled from "styled-components";
import { getCurrentRoomEvents } from "../../store/selectors";

class RoomEventsDisconnected extends Component {
  componentDidMount() {}

  render() {
    const {events} = this.props;
    console.log("EVENTS")
    console.log(events);
    return (
      <Layout>
        <h3>Events</h3>
        <List>
            {Object.values(events).map(event => <li key={event.event_id}>{event.type} - {event.sender}</li>)}
            </List>
      </Layout>
    );
  }
}

RoomEventsDisconnected.propTypes = {};

const mapStateToProps = state => {
  return {
      events: getCurrentRoomEvents(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export const RoomEvents = connect(
  mapStateToProps,
  mapDispatchToProps
)(RoomEventsDisconnected);

const Layout = styled.div`
  padding: 8px;
  background-color: var(--background-color);
  border-radius: 8px;
  min-height: 100px;
  flex: 2 1;
`;

const List = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
  color: var(--text-color);
  overflow-y: scroll;
  height: calc(100% - 30px);
`;

const ListItem = styled.li``;
