import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

export default class Message extends Component {
  render() {
    const { value, sender } = this.props;
    return <Body sender={sender}>{value}</Body>;
  }
}

const Body = styled.p`
  display: inline-block;
  background-color: ${props => props.sender ? "var(--accent-color-light)" : "var(--accent-color)"};
  color: ${props => props.sender ? "var(--text-color-dark)" : "white"};
  padding: 8px 15px;
  margin: 0;
  margin-bottom: 4px;
  border-radius: 8px;
  align-self: ${props => props.sender ? "flex-start" : "flex-end"};
  position: relative;

  &:last-child::after{
    content: "";
    display: block;
    position: absolute;
    height: 10px;
    bottom: -10px;
    left: 0;
    width: 100%;
  }
`;
