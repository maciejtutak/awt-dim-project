import React, { Component } from "react";
import styled from "styled-components";

import { RoomViewMembers } from "./RoomViewMembers";
import { RoomEvents } from "./RoomEvents";
import { Actions } from "./Actions";

export class RoomSidebar extends Component {
  render() {
    return (
      <Layout>
        <h2>Actions</h2>
        <Actions />
        <h2>Info</h2>
        <RoomViewMembers />
        <RoomEvents />
      </Layout>
    );
  }
}

const Layout = styled.div`
  padding: 10px;
  height: calc(100% - 25px);
  display: flex;
  flex-direction: column;

  & > * {
    margin-bottom: 20px;
  }
  
  & > :last-child {
    margin-bottom: 0;
  }
`;
