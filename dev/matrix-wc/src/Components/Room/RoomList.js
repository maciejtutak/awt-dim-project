import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styled from "styled-components";
import { syncDataActions } from "../../store/actions/syncDataActions";
import { getRoomNames, getRoomAliases, getCurrentRoom } from "../../store/selectors";

class RoomListDisconnected extends Component {
  handleClick = (room, event) => {
    const { setCurrentRoom } = this.props;
    setCurrentRoom(room);
  };

  displayRoomList(rooms) {
    const {currentRoomId} = this.props;
    return (
      <Fragment>
        <List>
          {Object.values(rooms).map(room => (
            <ListItem key={room} className={room == currentRoomId ? "active" : ""} onClick={this.handleClick.bind(this, room)}>
              {room}
            </ListItem>
          ))}
        </List>
      </Fragment>
    );
  }

  render() {
    const { rooms } = this.props;
    return (
      <Layout>
        <h2>Rooms</h2>
        {rooms && this.displayRoomList(rooms["join"])}
        <h2>Invitations</h2>
        {rooms && this.displayRoomList(rooms["invite"])}
      </Layout>
    );

    // return rooms ? (
    //   <Layout>
    //     {rooms["invite"] &&
    //       this.displayRoomList("Room Invites", rooms["invite"])}
    //     {rooms["join"] && this.displayRoomList("Joined Rooms", rooms["join"])}
    //     {rooms["leave"] && this.displayRoomList("Left Rooms", rooms["leave"])}
    //   </Layout>
    // ) : (
    //   <div>No room info to show.</div>
    // );
  }
}

RoomListDisconnected.propTypes = {
  rooms: PropTypes.objectOf(PropTypes.array)
};

const mapStateToProps = state => {
  return {
    rooms: getRoomNames(state),
    currentRoomId: getCurrentRoom(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setCurrentRoom: roomId => {
      dispatch(syncDataActions.setCurrentRoom(roomId));
    }
  };
};

export const RoomList = connect(
  mapStateToProps,
  mapDispatchToProps
)(RoomListDisconnected);

const Layout = styled.div`
  padding: 10px;
  display: flex;
  flex-flow: column nowrap;
  justify-content: flex-start;
`;

const List = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
  font-size: 14px;
  font-weight: 600;
`;

const ListItem = styled.li`
  margin: 2px 0;
  padding: 12px 8px;

  &:hover, &:active, &.active {
    color: white;
    background-color: var(--accent-color);
    border-radius: 4px;
  }

`;
