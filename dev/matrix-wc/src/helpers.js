export function handleResponse(response) {
    return response.json()
        .then(json => {
            const data = json;
            // console.log('handle response');
            // console.log(response);
            if (!response.ok) {
                // unauthorized
                if (response.status === 401) {
                    window.location.reload();
                }
                const error = response.statusText;
                return Promise.reject(error);
            }
            return data;
        });
}

export function constructParams(params) {
    return Object.keys(params).map(key => key + '=' + params[key]).join('&');
}

// https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
export function generateqQuickTId() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
}

export function getUsernameFromId(userId) {
    return userId.match(/@(.*):/)[1];
}