import { roomParticipationConstants } from "../constants";
import { roomParticipationServices } from "../../apiServices/roomParticipationServices";
import { getUserAccessToken, getCurrentRoom } from "../selectors";
import { roomEventActions } from "./roomEventActions";

export const roomParticipationActions = {
  invite,
  join,
  leave
};

function invite(userId) {
  return (dispatch, getState) => {
    const accessToken = getUserAccessToken(getState());
    const roomId = getCurrentRoom(getState());
    // console.log('INVITE ACTION')
    // console.log(accessToken, roomId, userId)
    dispatch(request());
    roomParticipationServices.invite(accessToken, roomId, userId).then(
      data => {
        dispatch(success(data));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request() {
    return { type: roomParticipationConstants.INVITE_USER_REQUEST };
  }

  function success(data) {
    return { type: roomParticipationConstants.INVITE_USER_SUCCESS, data };
  }

  function failure(error) {
    return { type: roomParticipationConstants.INVITE_USER_FAILURE, error };
  }
}

function join() {
  return (dispatch, getState) => {
    const accessToken = getUserAccessToken(getState());
    const roomId = getCurrentRoom(getState());

    dispatch(request());
    roomParticipationServices.join(accessToken, roomId).then(
      data => {
        dispatch(success(data));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request() {
    return { type: roomParticipationConstants.JOIN_ROOM_REUQEST };
  }

  function success(data) {
    return { type: roomParticipationConstants.JOIN_ROOM_SUCCESS, data };
  }

  function failure(error) {
    return { type: roomParticipationConstants.JOIN_ROOM_FAILURE, error };
  }
}

// BUG? once you leave the room and log out the room disappears from left rooms
function leave() {
  return (dispatch, getState) => {
    const accessToken = getUserAccessToken(getState());
    const roomId = getCurrentRoom(getState());

    dispatch(request());
    roomParticipationServices.leave(accessToken, roomId).then(
      data => {
        dispatch(success(data));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request() {
    return { type: roomParticipationConstants.LEAVE_ROOM_REQUEST };
  }

  function success(data) {
    return { type: roomParticipationConstants.LEAVE_ROOM_SUCCESS, data };
  }

  function failure(error) {
    return { type: roomParticipationConstants.LEAVE_ROOM_FAILURE, error };
  }
}

// function send(message) {
//   return (dispatch, getState) => {
//     const accessToken = getUserAccessToken(getState());
//     const roomId = getCurrentRoom(getState());

//     dispatch(request());
//     roomParticipationServices.send(accessToken, roomId, message).then(
//       data => {
//         dispatch(success(data));
//       },
//       error => {
//         dispatch(failure(error));
//       }
//     );
//   };

//   function request() {
//     return { type: roomParticipationConstants.SEND_MESSAGE_REQUEST };
//   }

//   function success(data) {
//     return { type: roomParticipationConstants.SEND_MESSAGE_SUCCESS, data };
//   }

//   function failure(error) {
//     return { type: roomParticipationConstants.SEND_MESSAGE_FAILURE, error };
//   }
// }

// function getRoomMembers() {
//     return (dispatch, getState) => {
//         const accessToken = getUserAccessToken(getState());
//         const roomId = getCurrentRoom(getState());

//         dispatch(request());
//         roomParticipationServices.fetchRoomMembers(roomId, accessToken)
//             .then(
//                 data => {
//                     dispatch(success(data))
//                 },
//                 error => {
//                     dispatch(failure(error))
//                 }
//             )
//     };

//     function request() {
//         return {type: roomParticipationConstants.GET_ROOM_MEMBERS_REQUEST}
//     }

//     function success(membersData) {
//         return {type: roomParticipationConstants.GET_ROOM_MEMBERS_SUCCESS, membersData}
//     }

//     function failure(error) {
//         return {type: roomParticipationConstants.GET_ROOM_MEMBERS_FAILURE, error}
//     }
// }
