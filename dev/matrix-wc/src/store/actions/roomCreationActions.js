import {
  roomCreationConstants,
  syncDataConstants
} from "../constants";
import { roomCreationServices } from "../../apiServices/roomCreationServices";
import { getUserAccessToken } from "../selectors";

export const roomCreationActions = {
  createRoom
};

function createRoom(alias) {
  return (dispatch, getState) => {
    const accessToken = getUserAccessToken(getState());
    dispatch(request());

    roomCreationServices.createRoom(accessToken, alias).then(
      room => {
        dispatch(success(room));
        dispatch(setCurrentRoom(room));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request() {
    return { type: roomCreationConstants.ROOM_CREATE_REQUEST };
  }

  function success(room) {
    return { type: roomCreationConstants.ROOM_CREATE_SUCCESS, room };
  }

  function setCurrentRoom(room) {
    const roomId = room.room_id;
    return { type: syncDataConstants.SET_CURRENT_ROOM, roomId };
  }

  function failure(error) {
    return { type: roomCreationConstants.ROOM_CREATE_FAILURE, error };
  }
}
