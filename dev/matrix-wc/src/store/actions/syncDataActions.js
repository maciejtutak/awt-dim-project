import { syncDataConstants } from "../constants";
import { syncDataServices } from "../../apiServices/syncDataServices";
import { getUserAccessToken, getNextBatch, getCurrentRoom, getRoomNames } from "../selectors";
import { roomEventActions } from "./roomEventActions";

export const syncDataActions = {
  sync,
  setCurrentRoom
};

function sync() {
  return (dispatch, getState) => {
    const accessToken = getUserAccessToken(getState());
    const nextBatch = getNextBatch(getState());

    dispatch(request());
    syncDataServices.sync(accessToken, nextBatch).then(
      data => {
        dispatch(success(data));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request() {
    return { type: syncDataConstants.SYNC_REQUEST };
  }

  function success(data) {
    return { type: syncDataConstants.SYNC_SUCCESS, data };
  }

  function failure(error) {
    return { type: syncDataConstants.SYNC_FAILURE, error };
  }
}

function setCurrentRoom(roomId) {
  return (dispatch, getState) => {
    const { join } = getRoomNames(getState());
    const allowed = join.includes(roomId);

    // check to prevent room rerendering
    if (roomId !== getCurrentRoom(getState())) {
      dispatch(setCurrentRoom(allowed));
      dispatch(roomEventActions.getRoomMembers());
      // on initial load, load 10 messeges backward
      dispatch(roomEventActions.getRoomMessagesB());
    }
  };

  function setCurrentRoom(allowed) {
    return { type: syncDataConstants.SET_CURRENT_ROOM, roomId, allowed};
  }
}
