import {
  sessionManagementConstants,
  userSettingsConstants
} from "../constants";
import { sessionManagementServices } from "../../apiServices/sessionManagementServices";
import { navigate } from "@reach/router";
import { getUserAccessToken } from "../selectors";

export const sessionManagementActions = {
  login,
  logout
};

function login(user, password, type) {
  return dispatch => {
    dispatch(request());
    sessionManagementServices.login(user, password, type).then(
      user => {
        dispatch(success(user));
        navigate("/");
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request() {
    return { type: sessionManagementConstants.LOGIN_REQUEST };
  }

  function success(user) {
    return { type: sessionManagementConstants.LOGIN_SUCCESS, user };
  }

  function failure(error) {
    return { type: sessionManagementConstants.LOGIN_FAILURE, error };
  }
}

function logout() {
  return (dispatch, getState) => {
    const accessToken = getUserAccessToken(getState());

    dispatch(request());
    sessionManagementServices.logout(accessToken).then(
      user => {
        dispatch(success(user));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request() {
    return { type: sessionManagementConstants.LOGOUT_REQUEST };
  }

  // successful logout returns empty object ({}) from the server
  function success(user) {
    return { type: sessionManagementConstants.LOGOUT_SUCCESS, user };
  }

  function failure(error) {
    return { type: sessionManagementConstants.LOGOUT_FAILURE, error };
  }
}
