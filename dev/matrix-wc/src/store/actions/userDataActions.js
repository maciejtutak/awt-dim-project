import { userDataConstants } from "../constants";
import { userDataServices } from "../../apiServices/userDataServices";
import { navigate } from "@reach/router";

export const userDataActions = {
  register
};

function register(username, password, auth) {
  return dispatch => {
    dispatch(request({ username }));
    userDataServices.register(username, password, auth).then(
      user => {
        dispatch(success(user));
        navigate("/");
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request(user) {
    return { type: userDataConstants.REGISTER_REQUEST, user };
  }

  function success(user) {
    return { type: userDataConstants.REGISTER_SUCCESS, user };
  }

  function failure(error) {
    return { type: userDataConstants.REGISTER_FAILURE, error };
  }
}
