import { roomEventConstants } from "../constants";
import { roomEventServices } from "../../apiServices/roomEventServices";
import {
  getUserAccessToken,
  getCurrentRoom,
  getFromEnd,
  getPrevBatchForCurrentRoom,
  getFromStart
} from "../selectors";

export const roomEventActions = {
  send,
  getRoomMessagesF,
  getRoomMessagesB,
  getRoomMembers
};

function send(message) {
  return (dispatch, getState) => {
    const accessToken = getUserAccessToken(getState());
    const roomId = getCurrentRoom(getState());

    dispatch(request());
    roomEventServices.send(accessToken, roomId, message).then(
      data => {
        dispatch(success(data));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request() {
    return { type: roomEventConstants.SEND_MESSAGE_REQUEST };
  }

  function success(data) {
    return { type: roomEventConstants.SEND_MESSAGE_SUCCESS, data };
  }

  function failure(error) {
    return { type: roomEventConstants.SEND_MESSAGE_FAILURE, error };
  }
}

function getRoomMessages(dir) {
  return (dispatch, getState) => {
    const accessToken = getUserAccessToken(getState());
    const roomId = getCurrentRoom(getState());
    const fromWhen =
      dir === "b" ? getFromStart(getState()) : getFromEnd(getState());
    const from =
      fromWhen !== "" ? fromWhen : getPrevBatchForCurrentRoom(getState());

    dispatch(request());
    roomEventServices.getRoomMessages(accessToken, roomId, from, dir).then(
      data => {
        dispatch(success(data, dir));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request(dir) {
    return dir === "b"
      ? {
          type: roomEventConstants.GET_ROOM_MESSAGES_REQUEST_B
        }
      : {
          type: roomEventConstants.GET_ROOM_MESSAGES_REQUEST_F
        };
  }

  function success(messagesResponse, dir) {
    return dir === "b"
      ? {
          type: roomEventConstants.GET_ROOM_MESSAGES_SUCCESS_B,
          messagesResponse
        }
      : {
          type: roomEventConstants.GET_ROOM_MESSAGES_SUCCESS_F,
          messagesResponse
        };
  }

  function failure(error) {
    return { type: roomEventConstants.GET_ROOM_MESSAGES_FAILURE, error };
  }
}

function getRoomMessagesB() {
  return getRoomMessages("b");
}

function getRoomMessagesF() {
  return getRoomMessages("f");
}

function getRoomMembers() {
  return (dispatch, getState) => {
    const accessToken = getUserAccessToken(getState());
    const roomId = getCurrentRoom(getState());

    dispatch(request());
    roomEventServices.getRoomMembers(accessToken, roomId).then(
      data => {
        dispatch(success(data));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request() {
    return { type: roomEventConstants.GET_ROOM_MEMBERS_REQUEST };
  }

  function success(membersResponse) {
    return {
      type: roomEventConstants.GET_ROOM_MEMBERS_SUCCESS,
      membersResponse
    };
  }

  function failure(error) {
    return { type: roomEventConstants.GET_ROOM_MEMBERS_FAILURE, error };
  }
}
