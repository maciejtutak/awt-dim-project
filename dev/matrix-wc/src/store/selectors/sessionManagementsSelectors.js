import {createSelector} from 'reselect';

export const getUserAccessToken = state => state.sessionManagementReducer.user.access_token;
export const isAuthenticated = state => Boolean(getUserAccessToken(state));
export const getUserId = state => state.sessionManagementReducer.user.user_id;
export const getUsername = createSelector(
    [getUserId],
    userId => {
        return userId.match(/@(.*):/)[1];
    }
);
