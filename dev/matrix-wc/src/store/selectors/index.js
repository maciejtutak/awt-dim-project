export * from './sessionManagementsSelectors';
export * from './roomParticipationSelectors';
export * from './roomEventSelectors';
export * from './syncDataSelectors';

export const getServerUrl = state => state.userSettingsReducer.homeserver;