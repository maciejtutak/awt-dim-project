import { createSelector } from "reselect";

export const getRoomMembers = state => state.roomEventReducer.members || [];

export const getRoomMessages = state =>
  state.roomEventReducer.messages ? state.roomEventReducer.messages.reverse() : [];

export const getIsLoadingData = state => state.roomEventReducer.isLoadingData;

export const getFromEnd = state => state.roomEventReducer.end;

export const getFromStart = state => state.roomEventReducer.start;
