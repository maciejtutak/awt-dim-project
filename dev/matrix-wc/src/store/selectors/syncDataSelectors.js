import { createSelector } from "reselect";

export const getRooms = state => state.syncDataReducer.rooms;
export const getNextBatch = state => state.syncDataReducer.next_batch;


// export const getCurrentRoom = state => Object.keys(state.syncDataReducer.currentRoom);
export const getCurrentRoom = state => state.syncDataReducer.currentRoomId;
export const getCurrentRoomAllowed = state => state.syncDataReducer.currentRoomAllowed;

export const getAllRooms = createSelector(
  [getRooms],
  rooms => {
    const { invite, join, leave } = rooms;
    return { ...invite, ...join, ...leave };
  }
);

export const getPrevBatchForCurrentRoom = state => state.syncDataReducer.next_batch;

// export const getPrevBatchForCurrentRoom = createSelector(
//   [getAllRooms, getCurrentRoom],
//   (allRooms, currentRoomId) => {
//     return Object.keys(allRooms).find(key => key === currentRoomId)
//       ? allRooms[currentRoomId].timeline.prev_batch
//       : null;
//   }
// );

export const getRoomNames = createSelector(
  [getRooms],
  rooms => {
    const { invite, join, leave } = rooms;
    return {
      invite: Object.keys(invite),
      join: Object.keys(join),
      leave: Object.keys(leave)
    };
  }
);

// export const getRoomAliases = createSelector(
//   [getRooms],
//   rooms => {
//     const { invite, join, leave } = rooms;
//     const a = Object.values(join).map(room => (
//       room.state.events.content
//     ))
//     return a;
//   }
// )

// here I want to parse the json sync response
// rooms: {
//  invite: {}
//  join: {}
//  leave: {}
// }

export const getCurrentRoomState = createSelector(
  [getRooms, getCurrentRoom],
  (rooms, currentRoomId) => {
    const { invite, join, leave } = rooms;
    const allRooms = { ...invite, ...join, ...leave };
    // console.log(allRooms);
    return Object.keys(allRooms).find(key => key === currentRoomId)
      ? allRooms[currentRoomId].state
      : null;
  }
);

export const getCurrentRoomEvents = createSelector(
  [getCurrentRoomState],
  state => (state ? state.events.sort((a, b) => (a.origin_server_ts - b.origin_server_ts)) : {})
);

// not all members are in events? on sync, the last event is not in state, but in timeline
// export const getCurrentRoomMembers = createSelector(
//   [getCurrentRoomEvents],
//   events =>
//     Object.values(events)
//       .filter(event => event.type === "m.room.member")
//       .map(event => event.content.displayname)
// );
