import {
  roomEventConstants,
  sessionManagementConstants,
  syncDataConstants
} from "../constants";

const initialState = {
  members: [],
  // messagesResponse: { chunk: [], end: "", start: "" },
  messages: [],
  start: "",
  end: "",
  isLoadingMessages: false,
  error: "",
};

export function roomEventReducer(state = initialState, action) {
  switch (action.type) {
    case roomEventConstants.GET_ROOM_MEMBERS_REQUEST:
      return {
        ...state
      };
    case roomEventConstants.GET_ROOM_MEMBERS_SUCCESS:
      return {
        ...state,
        members: action.membersResponse.chunk
      };
    case roomEventConstants.GET_ROOM_MEMBERS_FAILURE:
      return {
        error: action.error
      };
    case roomEventConstants.GET_ROOM_MESSAGES_REQUEST_F:
      return {
        ...state,
      };
    case roomEventConstants.GET_ROOM_MESSAGES_REQUEST_B:
      return {
        ...state,
        isLoadingMessages: true
      };
    case roomEventConstants.GET_ROOM_MESSAGES_SUCCESS_F:
      return {
        ...state,
        // messagesResponse: action.messagesResponse,
        messages: [...action.messagesResponse.chunk, ...state.messages],
        end: action.messagesResponse.end,
        error: "",
      };
    case roomEventConstants.GET_ROOM_MESSAGES_SUCCESS_B:
      return {
        isLoadingMessages: false,
        ...state,
        // messagesResponse: action.messagesResponse,
        messages: [...state.messages, ...action.messagesResponse.chunk],
        start: action.messagesResponse.end,
        error: "",
      };
    case roomEventConstants.GET_ROOM_MESSAGES_FAILURE:
      return {
        error: action.error
      };
    // case roomParticipationConstants.LEAVE_ROOM_SUCCESS:
    //   return {
    //     ...initialState,
    //   }
    case syncDataConstants.SET_CURRENT_ROOM:
      return {
        ...initialState
      };
    case sessionManagementConstants.LOGOUT_SUCCESS:
      return {
        ...initialState
      };
    default:
      return {
        ...state
      };
  }
}
