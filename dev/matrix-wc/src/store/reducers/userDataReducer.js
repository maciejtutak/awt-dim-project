import { userDataConstants } from "../constants";

const initialState = { user: {} };

export function userDataReducer(state = initialState, action) {
  switch (action.type) {
    case userDataConstants.REGISTER_REQUEST:
      return {
        ...state,
        user: action.user
      };
    case userDataConstants.REGISTER_SUCCESS:
      return {
        ...state,
        user: action.user
      };
    case userDataConstants.REGISTER_FAILURE:
      return {
        error: action.error
      };
    default:
      return state;
  }
}
