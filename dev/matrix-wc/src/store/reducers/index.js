import { combineReducers } from "redux";
import { sessionManagementReducer } from "./sessionManagementReducer";
import { userDataReducer } from "./userDataReducer";
import { syncDataReducer } from "./syncDataReducer";
import { roomParticipationReducer } from "./roomParticipationReducer";
import { roomCreationReducer } from "./roomCreationReducer";
import { roomEventReducer } from "./roomEventReducer";
// import { userSettingsReducer } from "./userSettingsReducer";

const rootReducer = combineReducers({
  sessionManagementReducer,
  userDataReducer,
  syncDataReducer,
  roomParticipationReducer,
  roomCreationReducer,
  roomEventReducer,
  // userSettingsReducer
});

export default rootReducer;
