import { roomCreationConstants } from "../constants";

const initialState = {};

export function roomCreationReducer(state = initialState, action) {
  switch (action.type) {
    case roomCreationConstants.ROOM_CREATE_REQUEST:
      return {};
    case roomCreationConstants.ROOM_CREATE_SUCCESS:
      return {
        room: action.room
      };
    case roomCreationConstants.ROOM_CREATE_FAILURE:
      return {
        error: action.error
      };
    default:
      return {};
  }
}
