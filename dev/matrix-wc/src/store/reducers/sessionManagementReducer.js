import { sessionManagementConstants, userDataConstants } from "../constants";

let user = JSON.parse(localStorage.getItem("user"));

const initialState = user ? { user: user } : { user: { access_token: "" } };

export function sessionManagementReducer(state = initialState, action) {
  switch (action.type) {
    case sessionManagementConstants.LOGIN_REQUEST:
      return {
      };
    case userDataConstants.REGISTER_SUCCESS:
    case sessionManagementConstants.LOGIN_SUCCESS:
      console.log('ACTION')
      console.log('ACTION')
      console.log('ACTION')
      console.log(action)
      return {
        user: action.user
      };
    case sessionManagementConstants.LOGIN_FAILURE:
      return {
        error: action.error
      };
    case sessionManagementConstants.LOGOUT_REQUEST:
      return {
        ...state
      };
    case sessionManagementConstants.LOGOUT_SUCCESS:
      return {
        user: action.user
      };
    case sessionManagementConstants.LOGOUT_FAILURE:
      return {
        error: action.error
      };
    default:
      return state;
  }
}
