import {
  syncDataConstants,
  sessionManagementConstants,
  roomParticipationConstants
} from "../constants";
import merge from "lodash/merge";

const initialState = {
  currentRoomId: "",
  currentRoomAllowed: false,
  account_data: {
    events: []
  },
  next_batch: "",
  presence: {
    events: []
  },
  rooms: {
    invite: {},
    join: {},
    leave: {}
  }
};

export function syncDataReducer(state = initialState, action) {
  switch (action.type) {
    case syncDataConstants.SYNC_REQUEST:
      return {
        ...state
      };
    case syncDataConstants.SYNC_SUCCESS:
      // might cause performance issues, works for now
      return merge({}, state, action.data);
    case syncDataConstants.SYNC_FAILURE:
      return {
        ...state,
        error: action.error
      };
    case syncDataConstants.SET_CURRENT_ROOM:
      return {
        ...state,
        currentRoomId: action.roomId,
        currentRoomAllowed: action.allowed
      };
    case roomParticipationConstants.JOIN_ROOM_SUCCESS:
      return {
        ...state,
        currentRoomAllowed: true
      };
    case roomParticipationConstants.LEAVE_ROOM_SUCCESS:
      return {
        ...state,
        currentRoomId: "",
        currentRoomAllowed: false,
      };
    case sessionManagementConstants.LOGOUT_SUCCESS:
      return {
        ...initialState,
      };
    default:
      return {
        ...state
      };
  }
}
