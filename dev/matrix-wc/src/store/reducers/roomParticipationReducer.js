import {
  roomParticipationConstants,
  sessionManagementConstants
} from "../constants";

const initialState = {};

export function roomParticipationReducer(state = initialState, action) {
  switch (action.type) {
    case roomParticipationConstants.INVITE_USER_REQUEST:
    case roomParticipationConstants.INVITE_USER_SUCCESS:
      return {
        ...state
      };
    case roomParticipationConstants.INVITE_USER_FAILURE:
      return {
        ...state,
        error: action.arror
      };
    case sessionManagementConstants.LOGOUT_SUCCESS:
      return {};
    // case roomCreationConstants.ROOM_CREATE_SUCCESS:
    //     console.log('CREATE')
    //     console.log(action.room.room_id)
    //     return {
    //         ...state,
    //         rooms: {
    //             ...state.rooms,
    //             join: [
    //                 state.rooms.join,
    //                 ...action.room.room_id
    //             ]
    //         }
    //     }
    default:
      return state;
  }
}
